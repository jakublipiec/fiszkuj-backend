const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');

const app = express()
                .use(bodyParser.json())
                .use(bodyParser.urlencoded({
                    extended: false
                }))
                .use(morgan('dev'))
                .use(cors())
                .get('/', (request, response, next) => {
                    response.json({
                        user: 'Ja'
                    })
                })
                .listen(config.port, () => {
                    console.log(`Application is running on port ${config.port}`);
                });

mongoose.connect(config.database, { useNewUrlParser: true }, (err) => {
    err ? console.log(err) : console.log("Connected to database");
})